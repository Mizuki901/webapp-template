describe('HomeControllerのテスト', function() {
  // HomeControllerのスコープを格納するための変数
  var scope;

  // モジュールを有効化
  beforeEach(module('App'));

  // $rootScope／$controllerサービスの注入
  beforeEach(inject(function(_$rootScope_, _$controller_){
    var $rootScope = _$rootScope_;
    var $controller = _$controller_;
    // テスト対象のコントローラーを初期化
    scope = $rootScope.$new();
    $controller('HomeController', { $scope: scope });
  }));

  it('スコープのテスト', function() {
      expect(scope.msg).toEqual('This is the $scope test.');
  });
});
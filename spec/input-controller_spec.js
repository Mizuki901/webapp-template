describe('InputControllerのテスト', function() {
  // InputControllerのスコープを格納するための変数
  var scope;

  // モジュールを有効化
  beforeEach(module('App'));

  // $rootScope／$controllerサービスの注入
  beforeEach(inject(function(_$rootScope_, _$controller_){
    var $rootScope = _$rootScope_;
    var $controller = _$controller_;
    // テスト対象のコントローラーを初期化
    scope = $rootScope.$new();
    $controller('InputController', { $scope: scope });
  }));

  it('send()の正常系テスト', function() {
      // messageの初期値を確認
      expect(scope.message).toEqual(undefined);
      // yourNameに値を設定してsendボタンを押下
      scope.yourName = 'Taro';
      scope.send();
      // messageの値が正しく更新されているか確認
      expect(scope.message).toEqual("Hello, Taro!");
  });

  it('yourNameがundefinedのときのsend()のテスト', function() {
      expect(scope.message).toEqual(undefined);
      scope.yourName = undefined;
      scope.send();
      expect(scope.message).toEqual(undefined);
  });

  it('yourNameが空値のときのsend()のテスト', function() {
      expect(scope.message).toEqual(undefined);
      scope.yourName = "";
      scope.send();
      expect(scope.message).toEqual(undefined);
  });
});
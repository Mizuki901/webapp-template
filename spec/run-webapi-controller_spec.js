describe('RunWebapiControllerのテスト', function() {
  // RunWebapiControllerのスコープを格納するための変数
  var scope, $httpBackend;

  // モジュールを有効化
  beforeEach(module('App'));

  beforeEach(inject(function(_$rootScope_, _$controller_, _$httpBackend_, _$mdDialog_){
    // サービスの注入
    var $rootScope = _$rootScope_;
    var $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    mdDialog = _$mdDialog_;

    // mdDialogのテストを行うためのモック
    spyOn(mdDialog, 'show');
    mdDialog.show.and.callFake(function () {
      return {
        then: function (callBack) {
          callBack(true);
        }
      }
    });
    
    // テスト対象のコントローラーを初期化
    scope = $rootScope.$new();
    $controller('RunWebapiController', {
      $scope: scope,
      $mdDialog: mdDialog
    });
  }));

  it('$httpBackendモックを使ったテスト（成功時）', function() {
    expect(scope.result).toEqual(undefined);

    var expected = 'success';

    // $httpBackendモックを使ってWebapiの挙動を定義
    $httpBackend.expect('GET', 'http://example.com/uri')
    .respond(200, 'success');
    
    // 上記で定義したWebapiの挙動を再現する
    scope.runHttp();
    $httpBackend.flush();

    // Webapi実行後のresultの値を確認
    expect(scope.result).toEqual(expected);
  });

  it('$httpBackendモックを使ったテスト（失敗時）', function() {
    expect(scope.result).toEqual(undefined);

    // $httpBackendモックを使ってWebapiの挙動を定義
    $httpBackend.expect('GET', 'http://example.com/uri')
    .respond(404, '');
    
    // 上記で定義したWebapiの挙動を再現する
    scope.runHttp();
    $httpBackend.flush();

    // Webapi実行後のresultの値を確認
    expect(scope.result).toEqual('error occurred');

    // ダイアログが表示されることのテスト
    expect(mdDialog.show).toHaveBeenCalled();
  });
});
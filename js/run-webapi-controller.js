app.controller('RunWebapiController', function($scope, $http, $mdDialog) {
    $scope.runHttp = function() {
      $http({
        method: 'GET',
        url: 'http://example.com/uri'
      })
      // AngularJS 1.6.xより.success()や.error()が使えなくなり
      // onSuccessやonErrorによる書き方に変わったので注意
      .then(function onSuccess(response){
        var data = response.data;
        $scope.result = data;
      }, function onError(response){
        $scope.result = 'error occurred';

        // エラーダイアログを表示
        $mdDialog.show(
          $mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Error')
            .textContent('This is an error message.')
            .ok('ok')
        );
      });
    }
});

// Angular
var app = angular.module('App', ['ngSanitize', 'ngRoute', 'ngMaterial', 'ngAnimate', 'ngMessages'])

// Angular Global Config
app.config(function($routeProvider) {
    // Angular Route
    $routeProvider.when('/', {
        templateUrl: '../views/home.html',
        controller: 'HomeController'
    })
    .when('/input', {
        templateUrl: '../views/input.html',
        controller: 'InputController'
    })
    .when('/tabs', {
        templateUrl: '../views/tabs.html',
        controller: 'TabsController'
    })
    .when('/run-webapi', {
        templateUrl: '../views/run-webapi.html',
        controller: 'RunWebapiController'
    })
    .otherwise({
        redirectTo: '/'
    });

})
